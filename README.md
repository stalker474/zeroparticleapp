# ZeroParticleApp

Mobile app for the Zero Particle sensor project

## System Description Document

- ZeroParticle_SyD.md
Generic description of system's goals and features

## Software Requirement Document

- ZeroParticle_SwR.md
Technical software requirements

## Software Design Document

- ZeroParticle_SwD.md
Detailed software design and conception

## Test and Validation Document

- ZeroParticle_TV.md
Manual and automatic validation of the software solution

## User Manual

- ZeroParticle_UM.md