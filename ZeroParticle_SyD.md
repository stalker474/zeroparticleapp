# Zero Particle System Description Document

## Revisions

| Author | Description                         | Date     |
|--------|-------------------------------------|----------|
| AP     | Creation of the document            | 16/10/20 |

## System overview

The Zero Particle System is a mobile/web application (henceforth refered to as The Application).
The Application is a companion to a portable air quality measuring device (or The Device).

## Prior development

Currently, The Device already has a companion app developped with Android SDK and named "Particle".
The applicationi s composed of 4 views.
See "Additional data" for screenshots.

### View 1
- Allows to view a list of available bluetooth devices.
- Allows to select The Device from the list.
- Aknowledges a succesfull connection.
- Allows to disconnect from The Device.

### View 2
- Allows to see the real time PM10 and PM2.5 values measured from The Device.

### View 3
- Allows to see history of recorded data.
- Displays a list of recorded data with values and timestamp.
- The list also displays if the entry has GPS coordinates associated to it.

### View 4
- Allows to see the current location on a map + color coded dots of geolocalised historical data.

### Additional information
- The code source of the original application is available: [particle](https://gitlab.com/stalker474/particle)

## System functionality

### Communicating with the device
- The Device provides measurements of PM10 and PM2.5 levels upon request through the communication protocol.
- The Application shall signal communication issues with the device (out of battery or out of range).

### Storing air quality data
- The Application shall have a database for storing the air quality data smpled from The Device.
- It must record PM10 and PM2.5 levels as long as the timestamp and potentially the location data associated with the sample.

### Tracking GPS location
- The Application shall be able to track user location through high accuracy GPS.
- It must also allow the user to opt-out of GPS usage, in that case, the stored air quality data must not store the sample location data.

### Displaying real time data
- Show the most up to date pm10 and pm2.5 values

### Displaying historical data
- All sampled data must be accessible through a list and a map.
- The list must display all sampled data.
- The map must display data with location information.
- Each location must be represented on the map as a color-coded dot.

## Additional data

### View 1
![](images/view1.jpg)

### View 2
![](images/view2.jpg)

### View 3
![](images/view3.jpg)

### View 4
![](images/view4.jpg)