# Zero Particle Software Requirement Document

## Revisions

| Author | Description                         | Date     |
|--------|-------------------------------------|----------|
| AP     | Creation of the document            | 16/10/20 |

## Introduction

This document describes the software requirements of the system

## Overall description

The Application has few software requirements, but must be able to communicate with the existing Device.

## System Features and Requirements

### Compatibility
- SwR1 : The Application must be able to be deployed on a mobile device

### Communication
- SwR2 : The Application must be able to communicate through Bluetooth with The Device
- SwR3 : The application must be able to pull PM10 and PM2.5 levels every 5 seconds.
- SwR4 : The Application shall signal communication issues with the device (out of battery or out of range)

### User data
- SwR5 : The Application shall be able to track user location through high accuracy GPS.
- SwR6 : It must also allow the user to opt-out of GPS usage, in that case, the stored air quality data must not store the sample location data.

### Displaying
- SwR7 : All recorded data shall be visible in a list
- SwR8 : Recorded data with localization information shall be visible on a map
- SwR9 : The map must be centered on the user by default
- SwR10 : The map must be navigable
- SwR11 : Real time data must be visible

### Storage and export
- SwR12 : The Application must store all recorded data in a database
- SwR13 : The Application must provide CSV export of the data